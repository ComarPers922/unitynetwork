using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ProDebug
{
    public static class ProLogger
    {
        public static void Log(string content)
        {
            ConsoleProDebug.LogAsType(content, "Log");
        }

        public static void LogError(string content)
        {
            ConsoleProDebug.LogAsType(content, "Error");
        }

        public static void LogWarning(string content)
        {
            ConsoleProDebug.LogAsType(content, "Warning");
        }
    }
}
