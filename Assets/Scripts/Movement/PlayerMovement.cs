using System.Collections;
using System.Collections.Generic;
using Mirror;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.InputSystem;
using UnityEngine.PlayerLoop;

public class PlayerMovement : NetworkBehaviour
{
    private NavMeshAgent agent;
    private Camera mainCamera;

    [Command]
    private void CmdMove(Vector3 destination)
    {
        if (NavMesh.SamplePosition(destination, out var hit, 1f, NavMesh.AllAreas))
        {
            agent?.SetDestination(hit.position);
        }
    }

    public override void OnStartClient()
    {
        base.OnStartClient();
        agent = GetComponent<NavMeshAgent>();
        mainCamera = GameObject.FindGameObjectWithTag("MainCamera")?.GetComponent<Camera>();
    }

    [ClientCallback]
    private void FixedUpdate()
    {
        if (!hasAuthority)
        {
            return;
        }

        if (Mouse.current.leftButton.isPressed)
        {
            var ray = mainCamera.ScreenPointToRay(Mouse.current.position.ReadValue());
            if (Physics.Raycast(ray, out var hit, Mathf.Infinity))
            {
                CmdMove(hit.point);
            }
        }
    }
}
