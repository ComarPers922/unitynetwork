using System.Collections;
using System.Collections.Generic;
using Mirror;
using ProDebug;
using UnityEngine;

public class NetworkPawn : NetworkManager
{
    public override void OnClientConnect()
    {
        base.OnClientConnect();
        ProLogger.Log("A successful connection!");
    }

    public override void OnServerAddPlayer(NetworkConnectionToClient conn)
    {
        base.OnServerAddPlayer(conn);
        ProLogger.Log("A successful player addition!");
        ProLogger.Log($"Current Num of Players: {numPlayers}");
    }
}
